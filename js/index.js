import { MatchThree } from "./matchThree.js"

const COLUMS_AMOUNT = 8
const ROWS_AMOUNT = 8
const TILES_AMOUNT = 7

const startGame = () => {
    new MatchThree(COLUMS_AMOUNT, ROWS_AMOUNT, TILES_AMOUNT)
}

startGame()
