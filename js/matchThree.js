import { Game, Grid } from "./game/index.js"

export class MatchThree {
    wrap = document.querySelector(".wrap")
    restartButton = document.querySelector(".menu>button")

    constructor(rowsCount, columnsCount, tilesCount) {
        this.game = new Game(rowsCount, columnsCount, tilesCount)
        this.grid = new Grid(this.wrap, this.game.matrix)

        this.wrap.addEventListener("swap", (event) => {
            const { firstElementPosition, secondElementPosition } = event.detail

            this.swap(firstElementPosition, secondElementPosition)
        })

        this.restartButton.addEventListener("click", () => this.restart())
    }

    async swap(firstElementPosition, secondElementPosition) {
        const swapStates = this.game.swap(
            firstElementPosition,
            secondElementPosition
        )

        await this.grid.swap(
            firstElementPosition,
            secondElementPosition,
            swapStates
        )

        this.updateScore()
    }

    restart() {
        this.game.score = 0
        this.updateScore()
        this.game.init()
        this.grid.restart(this.game.matrix)
    }

    updateScore() {
        document.querySelector(".score").innerHTML = this.game.score
        console.log(this.game)
    }
}
