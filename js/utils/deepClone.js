export const deepClone = (array) => {
    return array.map((innerArray) => [...innerArray])
}
